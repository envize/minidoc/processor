import LexerBase from '../../src/lexer/LexerBase';
import ISubLexer from '../../src/lexer/sublexers/ISubLexer';
import Token from "../../src/lexer/Token";
import Context from '../../src/lexer/Context';
import { lex } from '../../src/lexer/Lexer';
import TokenType from '../../src/lexer/TokenType';
import { isolatedSuite, integratedSuite } from '../suites';

export type lex = <LexerType extends ISubLexer>(
    input: string,
    lexerTypes: (new (context: Context) => LexerType)[],
    expectedTokens?: TokenType[],
    maxIterations?: number | undefined
) => LexResult;

export class LexResult {
    constructor(
        public allTokens: Token[],
        public tokens: Token[]
    ) {}
}

export function createSimpleSuite(tests: () => void, ...suiteNames: string[]) {
    executeSuite(suiteNames, tests);
}

export function createSuite(tests: (lex: lex) => void, ...suiteNames: string[]) {
    executeSuite([...suiteNames, isolatedSuite], () => tests(lexIsolated));
    executeSuite([...suiteNames, integratedSuite], () => tests(lexIntegrated));
}

function executeSuite(suiteNames: string[], tests: () => void) {
    const calls = [tests];
    for (let i = suiteNames.length - 1; i >= 0; i--) {
        const name = suiteNames.pop();
        const lastIndex = calls.length - 1;
        const call = () => describe(name, calls[lastIndex]);
        calls.push(call);
    }

    calls.pop()();
}

export function lexIsolated<LexerType extends ISubLexer>(
    input: string,
    lexerTypes: (new (context: Context) => LexerType)[],
    expectedTokens?: TokenType[],
    maxIterations?: number
): LexResult {
    const tokens = new IsolationLexer(input, maxIterations, ...lexerTypes).lex();
    return new LexResult(tokens, filterTokens(tokens, expectedTokens));
}

export function lexIntegrated<LexerType extends ISubLexer>(
    input: string,
    lexerTypes: (new (context: Context) => LexerType)[],
    expectedTokens?: TokenType[],
    maxIterations?: number
): LexResult {
    const tokens = lex(input);
    return new LexResult(tokens, filterTokens(tokens, expectedTokens));
}

function filterTokens(tokens: Token[], tokenTypes: TokenType[]|undefined) {
    return tokenTypes !== undefined ?
        tokens.filter(token => tokenTypes.includes(token.type)) :
        tokens.filter(token => token.type !== TokenType.EOF) ;
}

class IsolationLexer<LexerType extends ISubLexer> extends LexerBase {
    private lexers: LexerType[] = [];

    constructor(
        input: string,
        private maxIterations = 100,
        ...lexerTypes: (new (context: Context) => LexerType)[]
    ) {
        super(new Context([ ...input ]));

        for (let type of lexerTypes) {
            this.lexers.push(new type(this.context));
        }
    }

    lex(): Token[] {
        let i = 0;
        while (!this.isEof() &&
            i++ < this.maxIterations) {

            for (let lexer of this.lexers) {
                if (lexer.lex().breakFromMainLoop) {
                    break;
                }
            }
        }

        if (i > this.maxIterations) {
            throw new Error('Infinite loop detected.')
        }

        return this.context.tokens;
    }
}