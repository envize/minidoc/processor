import { lexerSuite, integratedSuite } from '../suites';
import { lex } from '../../src/lexer/Lexer';
import TokenType from '../../src/lexer/TokenType';
import { createSimpleSuite } from './lexing';

createSimpleSuite(tests, lexerSuite, 'main', integratedSuite);

function tests() {

    test('expect null input to throw error', () => {
        // act
        const sut = () => lex(null);

        // assert
        expect(sut).toThrowError();
    });

    test('expect empty input to return only <EOF> token', () => {
        // arrange
        const input = '';

        // act
        const tokens = lex(input);

        // assert
        expect(tokens).toHaveLength(1);
        expect(tokens[0].type).toBe(TokenType.EOF);
    });
}
