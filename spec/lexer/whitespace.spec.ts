import { lexerSuite } from '../suites';
import { createSuite, lex } from './lexing';
import WhitespaceLexer from '../../src/lexer/sublexers/WhitespaceLexer';
import FreeTextLexer from '../../src/lexer/sublexers/FreeTextLexer';

createSuite(tests, lexerSuite, 'whitespace');

function tests(lex: lex) {    

    test('expect single whitespace to be consumed and ignored', () => {
        // arrange
        const input = ` `;

        // act
        const { tokens } = lex(input, [WhitespaceLexer]);
        
        // assert
        expect(tokens).toHaveLength(0);
    });

    test('expect multiple whitespaces to be consumed and ignored', () => {
        // arrange
        const input = `   `;

        // act
        const { tokens } = lex(input, [WhitespaceLexer]);
        
        // assert
        expect(tokens).toHaveLength(0);
    });

    test('expect whitespace surrounded text to be consumed and ignored', () => {
        // arrange
        const input = ` hey   `;

        // act
        const { tokens } = lex(input, [WhitespaceLexer, FreeTextLexer]);
        
        // assert
        expect(tokens).toHaveLength(1);
        expect(tokens[0].text).toHaveLength(3);
    });

    test('expect text surrounded whitespaces to be tokenized without the whitespaces', () => {
        // arrange
        const input = `hey   hey`;

        // act
        const { tokens } = lex(input, [WhitespaceLexer, FreeTextLexer]);
        
        // assert
        expect(tokens).toHaveLength(2);
        expect(tokens[0].text).toHaveLength(3);
        expect(tokens[1].text).toHaveLength(3);
    });
}
