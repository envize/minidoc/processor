import { lexerSuite } from '../suites';
import { createSuite, lex } from './lexing';
import NewLineLexer from '../../src/lexer/sublexers/NewLineLexer';
import TokenType from '../../src/lexer/TokenType';
import FreeTextLexer from '../../src/lexer/sublexers/FreeTextLexer';
import IndentLexer from '../../src/lexer/sublexers/IndentLexer';

createSuite(tests, lexerSuite, 'new-line');

function tests(lex: lex) {

    test('expect single linebreak to return single token', () => {
        // arrange
        const input = `\n`;

        // act
        const { tokens } = lex(input, [NewLineLexer]);

        // assert
        expect(tokens).toHaveLength(1);
    });

    test('expect multiple linebreaks to return multiple tokens', () => {
        const input = `\n\n\n`;
        // arrange

        // act
        const { tokens } = lex(input, [NewLineLexer]);

        // assert
        expect(tokens).toHaveLength(3);
    });

    test('expect linebreaks in between text to return token', () => {
        // arrange
        const input = `\n\n    hey\nhallo\n`;

        // act
        const { tokens } = lex(input, [NewLineLexer, FreeTextLexer, IndentLexer], [TokenType.NewLine]);

        // assert
        expect(tokens).toHaveLength(4);
    });
}