import { lexerSuite } from '../suites';
import { createSuite, lex } from './lexing';
import EndLexer from '../../src/lexer/sublexers/EndLexer';
import TokenType from '../../src/lexer/TokenType';
import WhitespaceLexer from '../../src/lexer/sublexers/WhitespaceLexer';
import FreeTextLexer from '../../src/lexer/sublexers/FreeTextLexer';
import NewLineLexer from '../../src/lexer/sublexers/NewLineLexer';
import IdentifierLexer from '../../src/lexer/sublexers/IdentifierLexer';

createSuite(tests, lexerSuite, 'identifier');

function tests(lex: lex) {

    test('expect id to return token', () => {
        // arrange
        const input = '@id';

        // act
        const { tokens } = lex(input, [IdentifierLexer]);

        // assert
        expect(tokens).toHaveLength(1);
    });

    test('expect id at the end of file to return token', () => {
        // arrange
        const input = 'hey @id';

        // act
        const { tokens } = lex(input, [IdentifierLexer, FreeTextLexer, WhitespaceLexer], [TokenType.Identifier]);

        // assert
        expect(tokens).toHaveLength(1);
    });

    test('expect id at the end of line to return token', () => {
        // arrange
        const input = 'hey @id\n';

        // act
        const { tokens } = lex(input, [IdentifierLexer, FreeTextLexer, WhitespaceLexer, NewLineLexer], [TokenType.Identifier]);

        // assert
        expect(tokens).toHaveLength(1);
        expect(tokens[0].text).not.toContain('\n');
    });

    test('expect id in between text to return token', () => {
        // arrange
        const input = 'hey @id hallo';

        // act
        const { tokens } = lex(input, [IdentifierLexer, FreeTextLexer, WhitespaceLexer], [TokenType.Identifier]);

        // assert
        expect(tokens).toHaveLength(1);
    });

    test.each(['id', 'ID', '13', 'id_', 'i-d', 'Id-', '1d', '_1d', 'i_DEN-t1fier'])
        (`expect id with special characters to return token '%s'`, id => {
            // arrange
            const input = `@${id}`;

            // act
            const { tokens } = lex(input, [IdentifierLexer]);

            // assert
            expect(tokens).toHaveLength(1);
        });

    test.each(['id#', 'ID$', '13@', 'id_%', 'i-d^', 'I&d-', '*1d', '_(1d', 'i_DEN)-t1fier', '@id', 'i\\d', '/id'])
        (`expect id with invalid characters to return nothing '%s'`, id => {
            // arrange
            const input = `@${id}`;

            // act
            const { tokens } = lex(input, [IdentifierLexer, FreeTextLexer], [TokenType.Identifier]);

            // assert
            expect(tokens).toHaveLength(0);
        });

    test('expect id after end keyword to return token', () => {
        // arrange
        const input = 'end @id';

        // act
        const { tokens } = lex(input, [IdentifierLexer, EndLexer, WhitespaceLexer], [TokenType.Identifier]);

        // assert
        expect(tokens).toHaveLength(1);
    });
}