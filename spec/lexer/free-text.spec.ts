import { lexerSuite } from '../suites';
import { createSuite, lex } from './lexing';
import TokenType from '../../src/lexer/TokenType';
import WhitespaceLexer from '../../src/lexer/sublexers/WhitespaceLexer';
import FreeTextLexer from '../../src/lexer/sublexers/FreeTextLexer';
import NewLineLexer from '../../src/lexer/sublexers/NewLineLexer';

createSuite(tests, lexerSuite, 'free-text');

function tests(lex: lex) {

    test('expect word to return single token', () => {
        // arrange
        const input = 'hey';

        // act
        const { tokens } = lex(input, [FreeTextLexer]);

        // assert
        expect(tokens).toHaveLength(1);
    });

    test('expect sentence to return multiple tokens', () => {
        // arrange
        const input = 'Never gonna give you up';

        // act
        const { tokens } = lex(input, [FreeTextLexer, WhitespaceLexer]);

        // assert
        expect(tokens).toHaveLength(5);
    });

    test('expect sentence with extra whitespaces to return multiple tokens', () => {
        // arrange
        const input = '  Never  gonna          let   you down ';

        // act
        const { tokens } = lex(input, [FreeTextLexer, WhitespaceLexer]);

        // assert
        expect(tokens).toHaveLength(5);
    });

    test('expect sentence with special characters to return multiple tokens', () => {
        // arrange
        const input = `We're n0 #5tr@n&ers t*_lov3`;

        // act
        const { tokens } = lex(input, [FreeTextLexer, WhitespaceLexer]);

        // assert
        expect(tokens).toHaveLength(4);
    });

    test('expect linebreaks not to be included in the tokens', () => {
        // arrange
        const input = `We're no\nstrangers to love
        You know the rules and so do I\n\n
        A full commitment's what I'm thinking of
        \nYou wouldn't\n\nget this from any other guy`;

        // act
        const { tokens } = lex(input, [FreeTextLexer, WhitespaceLexer, NewLineLexer], [TokenType.FreeText]);

        // assert
        expect(tokens).toHaveLength(28);
        tokens.forEach(token => 
            expect(token.text).not.toMatch(/\\n/)
        );
    });
}