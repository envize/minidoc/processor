import { lexerSuite } from '../suites';
import { createSuite, lex } from './lexing';
import TokenType from '../../src/lexer/TokenType';
import IndentLexer from '../../src/lexer/sublexers/IndentLexer';
import FreeTextLexer from '../../src/lexer/sublexers/FreeTextLexer';
import NewLineLexer from '../../src/lexer/sublexers/NewLineLexer';
import WhitespaceLexer from '../../src/lexer/sublexers/WhitespaceLexer';

const indent = '    ';

createSuite(tests, lexerSuite, 'indent');

function tests(lex: lex) {    

    test('expect single indent to return single token', () => {
        // arrange
        const input = `${indent}`;

        // act
        const { tokens } = lex(input, [IndentLexer]);

        // assert
        expect(tokens).toHaveLength(1);
        expect(tokens[0].type).toBe(TokenType.Indent);
    });

    test('expect double indent to return tokens', () => {
        // arrange
        const input = `${indent}${indent}`;

        // act
        const { tokens } = lex(input, [IndentLexer]);

        // assert
        expect(tokens).toHaveLength(2);
        expect(tokens[0].type).toBe(TokenType.Indent);
        expect(tokens[1].type).toBe(TokenType.Indent);
    });

    test('expect empty string to return nothing', () => {
        // arrange
        const input = '';

        // act
        const { tokens } = lex(input, [IndentLexer]);

        // assert
        expect(tokens).toHaveLength(0);
    });

    test.each([ ' ', '  ', '   ' ])
        (`expect too little whitespaces to return nothing '%s'`, (input) => {
        // act
        const { tokens } = lex(input, [IndentLexer, WhitespaceLexer]);

        // assert
        expect(tokens).toHaveLength(0);
    })

    test('expect indent after text to return nothing', () => {
        // arrange
        const input = `hey${indent}`;

        // act
        const { tokens } = lex(input, [IndentLexer, FreeTextLexer, WhitespaceLexer], [TokenType.Indent]);

        // assert
        expect(tokens).toHaveLength(0);
    });

    test('expect indent after linebreak to return token', () => {
        // arrange
        const input = `\n${indent}`;

        // act
        const { tokens } = lex(input, [IndentLexer, NewLineLexer], [TokenType.Indent]);

        // assert
        expect(tokens).toHaveLength(1);
    });
}
