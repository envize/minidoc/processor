import { lexerSuite } from '../suites';
import { createSuite, lex } from './lexing';
import TokenType from '../../src/lexer/TokenType';
import WhitespaceLexer from '../../src/lexer/sublexers/WhitespaceLexer';
import FreeTextLexer from '../../src/lexer/sublexers/FreeTextLexer';
import IndentLexer from '../../src/lexer/sublexers/IndentLexer';
import NewLineLexer from '../../src/lexer/sublexers/NewLineLexer';
import SectionLexer from '../../src/lexer/sublexers/SectionLexer';

const sections = ['title', 'general', 'story', 'happy-flow', 'alternate-flow'];

createSuite(tests, lexerSuite, 'section');

function tests(lex: lex) {

    test.each(sections)
        (`expect section to return token '#%s'`, section => {
        // arrange
        const input = `#${section}`;

        // act
        const { tokens } = lex(input, [SectionLexer]);

        // assert
        expect(tokens).toHaveLength(1);
    });

    test.each(sections)
        (`expect section after whitespace to return token '#%s'`, section => {
        // arrange
        const input = ` #${section}`;

        // act
        const { tokens } = lex(input, [SectionLexer, WhitespaceLexer]);

        // assert
        expect(tokens).toHaveLength(1);
    });

    test.each(sections)
        (`expect section after indent to return token '#%s'`, section => {
        // arrange
        const input = `    #${section}`;

        // act
        const { tokens } = lex(input, [SectionLexer, IndentLexer], [TokenType.Section]);

        // assert
        expect(tokens).toHaveLength(1);
    });

    test.each(sections)
        (`expect section before linebreak to return token '#%s'`, section => {
        // arrange
        const input = `#${section}\n`;

        // act
        const { tokens } = lex(input, [SectionLexer, NewLineLexer], [TokenType.Section]);

        // assert
        expect(tokens).toHaveLength(1);
    });

    test(`expect section with invalid identifier to return nothing`, () => {
        // arrange
        const input = `#hey`;

        // act
        const { tokens } = lex(input, [SectionLexer, FreeTextLexer], [TokenType.Section]);

        // assert
        expect(tokens).toHaveLength(0);
    });

    test.each(sections)
        (`expect section after text to return nothing '#%s'`, section => {
        // arrange
        const input = `hey #${section}`;

        // act
        const { tokens } = lex(input, [SectionLexer, WhitespaceLexer, FreeTextLexer], [TokenType.Section]);

        // assert
        expect(tokens).toHaveLength(0);
    });

    test.each(sections)
        (`expect section with space between hashtag to return nothing '#%s'`, section => {
        // arrange
        const input = `# ${section}`;

        // act
        const { tokens } = lex(input, [SectionLexer, FreeTextLexer, WhitespaceLexer], [TokenType.Section]);

        // assert
        expect(tokens).toHaveLength(0);
    });

    test.each(sections)
        (`expect section with double hashtag to return nothing '#%s'`, section => {
        // arrange
        const input = `##${section}`;

        // act
        const { tokens } = lex(input, [SectionLexer, FreeTextLexer], [TokenType.Section]);

        // assert
        expect(tokens).toHaveLength(0);
    });

    test.each(sections)
        (`expect section with extra character at the end to return nothing '#%s'`, section => {
        // arrange
        const input = `#${section}d`;

        // act
        const { tokens } = lex(input, [SectionLexer, FreeTextLexer], [TokenType.Section]);

        // assert
        expect(tokens).toHaveLength(0);
    });
}