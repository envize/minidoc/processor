import { interpreterSuite } from '../suites';
import { evaluate } from './evaluation';
import TitleInterpreter from '../../src/interpreter/subinterpreters/TitleInterpreter';

describe(interpreterSuite, () => {

    describe('title', () => {
        
        test('expect title section to be integrated when valid', () => {
            // arrange
            const input = '#title hey';

            // act
            const document = evaluate(TitleInterpreter, input);

            // assert
            expect(document.title).toBe('hey');
        });
        
        test('expect title section not to be integrated when no title provided', () => {
            // arrange
            const input = '#title';

            // act
            const document = evaluate(TitleInterpreter, input);

            // assert
            expect(document.title).toBeUndefined();
        });
        
        test('expect story section to be skipped', () => {
            // arrange
            const input = '#story\nhey';

            // act
            const document = evaluate(TitleInterpreter, input);

            // assert
            expect(document.title).toBeUndefined();
        });
        
        test('expect second title section to be ignored', () => {
            // arrange
            const input = '#title first\n#title second';

            // act
            const document = evaluate(TitleInterpreter, input);

            // assert
            expect(document.title).toBe('first');
        });
    })
});
