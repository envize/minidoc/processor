import { interpreterSuite } from '../suites';
import { evaluate } from './evaluation';
import GeneralInterpreter from '../../src/interpreter/subinterpreters/GeneralInterpreter';

describe(interpreterSuite, () => {

    describe('general', () => {
        
        test('expect general section to be integrated when valid', () => {
            // arrange
            const input = '#general\nhey';

            // act
            const document = evaluate(GeneralInterpreter, input);

            // assert
            expect(document.general).not.toBeNull();
            expect(document.general.content).toBe('hey');
        });
        
        test('expect general section not to be integrated when empty', () => {
            // arrange
            const input = '#general';

            // act
            const document = evaluate(GeneralInterpreter, input);

            // assert
            expect(document.general).toBeNull();
        });
        
        test('expect title section to be skipped', () => {
            // arrange
            const input = '#title hey';

            // act
            const document = evaluate(GeneralInterpreter, input);

            // assert
            expect(document.general).toBeNull();
        });
        
        test('expect second general section to be ignored', () => {
            // arrange
            const input = '#general\nfirst\n#general\nsecond';

            // act
            const document = evaluate(GeneralInterpreter, input);

            // assert
            expect(document.general).not.toBeNull();
            expect(document.general.content).toBe('first');
        });
    })
});
