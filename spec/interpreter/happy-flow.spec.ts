import { interpreterSuite } from '../suites';
import { evaluate } from './evaluation';
import HappyFlowInterpreter from '../../src/interpreter/subinterpreters/HappyFlowInterpreter';
import { FlowType } from '@minidoc/core';

describe(interpreterSuite, () => {

    describe('happy-flow', () => {

        test('expect happy flow section to be integrated when valid', () => {
            // arrange
            const input = '#happy-flow\nstep one\naction @id step two';

            // act
            const document = evaluate(HappyFlowInterpreter, input);

            // assert
            expect(document.happyFlow).not.toBeNull();
            expect(document.happyFlow.type).toBe(FlowType.HappyFlow);
            expect(document.happyFlow.description).toBeNull();
            expect(document.happyFlow.steps.length).toBe(2);
            expect(document.happyFlow.start).toBeNull();
            expect(document.happyFlow.end).toBeNull();
        });

        test('expect links to be ignored', () => {
            // arrange
            const input = '#happy-flow\nstart @id\nstep one\naction @id step two\nend @id';

            // act
            const document = evaluate(HappyFlowInterpreter, input);

            // assert
            expect(document.happyFlow).not.toBeNull();
            expect(document.happyFlow.type).toBe(FlowType.HappyFlow);
            expect(document.happyFlow.description).toBeNull();
            expect(document.happyFlow.steps.length).toBe(2);
            expect(document.happyFlow.start).toBeNull();
            expect(document.happyFlow.end).toBeNull();
        });

        test.each([
            '#happy-flow',
            '#happy-flow\nstart @id',
            '#happy-flow\nend @id',
            '#happy-flow\nstart @id\nend @id'
        ])
            (`expect happy flow section not to be integrated when invalid`, (input: string) => {
                // act
                const document = evaluate(HappyFlowInterpreter, input);

                // assert
                expect(document.happyFlow).toBeNull();
            });

        test('expect title section to be skipped', () => {
            // arrange
            const input = '#title hey';

            // act
            const document = evaluate(HappyFlowInterpreter, input);

            // assert
            expect(document.happyFlow).toBeNull();
        });

        test('expect second happy flow section to be ignored', () => {
            // arrange
            const input = '#happy-flow\nfirst\n#happy-flow\nsecond';

            // act
            const document = evaluate(HappyFlowInterpreter, input);

            // assert
            expect(document.happyFlow).not.toBeNull();
            expect(document.happyFlow.steps[0].description).toBe('first');
        });
    });
});
