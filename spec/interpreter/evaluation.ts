import { Document } from '@minidoc/core';
import ISubInterpreter from '../../src/interpreter/subinterpreters/ISubInterpreter';
import { lex } from '../../src/lexer/Lexer';
import { parse } from '../../src/parser/Parser';

export function evaluate<InterpreterType extends ISubInterpreter<any>>(type: new () => InterpreterType, input: string, document?: Document): Document {
    const tokens = lex(input);
    const ast = parse(tokens);
    const sections = ast.sections;

    if (!document) {
        document = new Document(undefined);
    }

    for (const section of sections) {
        if (section !== undefined &&
            section !== null) {
            const interpreter = new type();
            interpreter.eval(document, section);
            interpreter.finalize(document, section);
        }
    }

    return document;
}

// import { isolatedSuite, integratedSuite } from '../suites';
// import ISubInterpreter from '../../src/interpreter/subinterpreters/ISubInterpreter';
// import EvalResult from '../../src/interpreter/EvalResult';
// import { evaluate } from '../../src/interpreter/Interpreter';
// import AbstractSyntaxTree from '../../src/parser/AbstractSyntaxTree';
// import { lex } from '../../src/lexer/Lexer';
// import { parse } from '../../src/parser/Parser';

// export type EvaluationType = <InterpreterType extends ISubInterpreter<any>>(
//     input: string,
//     type: (new () => InterpreterType),
// ) => { result: EvalResult, document: Document };

// export function createSuite(tests: (evaluate: EvaluationType) => void, ...suiteNames: string[]) {
//     executeSuite([...suiteNames, isolatedSuite], () => tests(lexIsolated));
//     executeSuite([...suiteNames, integratedSuite], () => tests(lexIntegrated));
// }

// function evaluateIntegrated<InterpreterType extends ISubInterpreter<any>>(
//     input: string,
//     type: (new () => InterpreterType),
// ): { result: EvalResult, document: Document } {
//     const ast = createAst(input);
//     const document = evaluate(ast);
// }

// function createAst(input: string): AbstractSyntaxTree {
//     const tokens = lex(input);
//     const ast = parse(tokens);

//     return ast;
// }

// function executeSuite(suiteNames: string[], tests: () => void) {
//     const calls = [tests];
//     for (let i = suiteNames.length - 1; i >= 0; i--) {
//         const name = suiteNames.pop();
//         const lastIndex = calls.length - 1;
//         const call = () => describe(name, calls[lastIndex]);
//         calls.push(call);
//     }

//     calls.pop()();
// }