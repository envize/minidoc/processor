import { interpreterSuite } from '../suites';
import { evaluate } from './evaluation';
import StoryInterpreter from '../../src/interpreter/subinterpreters/StoryInterpreter';

describe(interpreterSuite, () => {

    describe('story', () => {
        
        test('expect story section to be integrated when valid', () => {
            // arrange
            const input = '#story\nhey';

            // act
            const document = evaluate(StoryInterpreter, input);

            // assert
            expect(document.story).not.toBeNull();
            expect(document.story.content).toBe('hey');
        });
        
        test('expect story section not to be integrated when empty', () => {
            // arrange
            const input = '#story';

            // act
            const document = evaluate(StoryInterpreter, input);

            // assert
            expect(document.story).toBeNull();
        });
        
        test('expect title section to be skipped', () => {
            // arrange
            const input = '#title hey';

            // act
            const document = evaluate(StoryInterpreter, input);

            // assert
            expect(document.story).toBeNull();
        });
        
        test('expect second story section to be ignored', () => {
            // arrange
            const input = '#story\nfirst\n#story\nsecond';

            // act
            const document = evaluate(StoryInterpreter, input);

            // assert
            expect(document.story).not.toBeNull();
            expect(document.story.content).toBe('first');
        });
    });
});
