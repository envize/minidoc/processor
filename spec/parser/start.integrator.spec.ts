import { integratorSuite } from '../suites';
import { parserSuite } from '../suites';
import FlowSectionNode from '../../src/parser/nodes/FlowSectionNode';
import FlowType from '../../src/parser/nodes/FlowType';
import { createContext } from './parsing';
import StartParser from '../../src/parser/subparsers/StartParser';

const start = 'start';
const id = '@id';

function createParentNode() {
    return new FlowSectionNode(FlowType.AlternateFlow, 'test flow');
}

describe(parserSuite, () => {

    describe('start', () => {

        describe(integratorSuite, () => {

            test('expect start link to be integrated when parsed successfully', () => {
                // arrange
                const parser = new StartParser(createContext(`${start} ${id}`));
                const result = parser.parse(undefined);
                const parentNode = createParentNode();

                // act
                parser.integrate(result, parentNode);

                // assert
                expect(parentNode.start).not.toBeNull();
                expect(parentNode.start).toStrictEqual(result.node);
            });

            test('expect start link not to be integrated when not parsed', () => {
                // arrange
                const parser = new StartParser(createContext('invalid'));
                const result = parser.parse(undefined);
                const parentNode = createParentNode();

                // act
                parser.integrate(result, parentNode);

                // assert
                expect(parentNode.start).toBeNull();
            });
        });
    });
});
