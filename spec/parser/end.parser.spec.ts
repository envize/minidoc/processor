import { parserSuite, isolatedSuite } from '../suites';
import EndParser from '../../src/parser/subparsers/EndParser';
import LinkType from '../../src/parser/nodes/LinkType';
import { createContext } from './parsing';

const end = 'end';
const id = '@id';

describe(parserSuite, () => {

    describe('end', () => {

        describe(parserSuite, () => {

            test('expect end followed by id token to be parsed', () => {
                // arrange
                const parser = new EndParser(createContext(`${end} ${id}`));

                // act
                const result = parser.parse(undefined);

                // assert
                expect(result.shouldBeIntegrated).toBeTruthy();
                expect(result.breakFromMainLoop).toBeTruthy();
                expect(result.node).not.toBeUndefined();
                expect(result.node).not.toBeNull();
                expect(result.node.type).toBe(LinkType.End);
                expect(result.node.identifier).toBe('@id');
                expect(result.node.description).toBeNull();
            });

            test('expect end followed by id and description token to be parsed', () => {
                // arrange
                const parser = new EndParser(createContext(`${end} ${id} multi word description`));

                // act
                const result = parser.parse(undefined);

                // assert
                expect(result.shouldBeIntegrated).toBeTruthy();
                expect(result.breakFromMainLoop).toBeTruthy();
                expect(result.node).not.toBeUndefined();
                expect(result.node).not.toBeNull();
                expect(result.node.type).toBe(LinkType.End);
                expect(result.node.identifier).toBe('@id');
                expect(result.node.description).toBe('multi word description');
            });

            test('expect end without id token not to be parsed', () => {
                // arrange
                const parser = new EndParser(createContext(end));

                // act
                const result = parser.parse(undefined);

                // assert
                expect(result.shouldBeIntegrated).toBeFalsy();
                expect(result.breakFromMainLoop).toBeFalsy();
                expect(result.node).toBeUndefined();
            });

            test('expect id followed by end token not to be parsed', () => {
                // arrange
                const parser = new EndParser(createContext(`${id} ${end}`));

                // act
                const result = parser.parse(undefined);

                // assert
                expect(result.shouldBeIntegrated).toBeFalsy();
                expect(result.breakFromMainLoop).toBeFalsy();
                expect(result.node).toBeUndefined();
            });
        });
    });
});
