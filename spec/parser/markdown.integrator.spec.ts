import { integratorSuite } from '../suites';
import { parserSuite } from '../suites';
import { createContext } from './parsing';
import SectionNode from '../../src/parser/nodes/SectionNode';
import SectionType from '../../src/parser/nodes/Section';
import MarkdownSectionParser from '../../src/parser/subparsers/MarkdownSectionParser';

function createParentNode() {
    return new SectionNode(SectionType.General, '#general');
}

describe(parserSuite, () => {

    describe('markdown', () => {

        describe(integratorSuite, () => {

            test('expect flow to be integrated when parsed successfully', () => {
                // arrange
                const parser = new MarkdownSectionParser(createContext('\nhey'));
                const result = parser.parse(SectionType.General);
                const parentNode = createParentNode();

                // act
                parser.integrate(result, parentNode);

                // assert
                expect(parentNode.section).not.toBeNull();
                expect(parentNode.section).toStrictEqual(result.node);
            });

            test('expect flow not to be integrated when not parsed', () => {
                // arrange
                const parser = new MarkdownSectionParser(createContext('hey'));
                const result = parser.parse(SectionType.General);
                const parentNode = createParentNode();

                // act
                parser.integrate(result, parentNode);

                // assert
                expect(parentNode.section).toBeNull();
            });
        });
    });
});
