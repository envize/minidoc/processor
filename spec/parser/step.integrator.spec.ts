import { integratorSuite } from '../suites';
import { parserSuite } from '../suites';
import FlowSectionNode from '../../src/parser/nodes/FlowSectionNode';
import FlowType from '../../src/parser/nodes/FlowType';
import { createContext } from './parsing';
import StepParser from '../../src/parser/subparsers/StepParser';

function createParentNode() {
    return new FlowSectionNode(FlowType.AlternateFlow, 'test flow');
}

describe(parserSuite, () => {

    describe('step', () => {

        describe(integratorSuite, () => {

            test('expect step to be integrated when parsed successfully', () => {
                // arrange
                const parser = new StepParser(createContext('hello I\'m a step!'));
                const result = parser.parse(undefined);
                const parentNode = createParentNode();

                // act
                parser.integrate(result, parentNode);

                // assert
                expect(parentNode.steps.length).toBe(1);
                expect(parentNode.steps[0]).toStrictEqual(result.node);
            });

            test('expect step not to be integrated when not parsed', () => {
                // arrange
                const parser = new StepParser(createContext(''));
                const result = parser.parse(undefined);
                const parentNode = createParentNode();

                // act
                parser.integrate(result, parentNode);

                // assert
                expect(parentNode.steps.length).toBe(0);
            });
        });
    });
});
