import Context from "../../src/parser/Context";
import { lex } from '../../src/lexer/Lexer';

export function createContext(input: string): Context {
    const tokens = lex(input);
    const context = new Context(tokens);

    return context;
}