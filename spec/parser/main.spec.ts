import { lex } from '../../src/lexer/Lexer';
import { parse } from '../../src/parser/Parser';
import { parserSuite } from '../suites';

describe(parserSuite, () => {

    describe('main', () => {
        
        test('expect null input to throw error', () => {
            // act
            const sut = () => parse(null);
    
            // assert
            expect(sut).toThrowError();
        });
        
        test('expect undefined input to throw error', () => {
            // act
            const sut = () => parse(undefined);
    
            // assert
            expect(sut).toThrowError();
        });
        
        test('expect valid input be parsed correctly', () => {
            // arrange
            const tokens = lex('#title hey\n#general\nhey');

            // act
            const ast = parse(tokens);
    
            // assert
            expect(ast).not.toBeNull();
            expect(ast).not.toBeUndefined();
            expect(ast.sections.length).toBe(2);
        });
        
        test('expect input without sections be parsed as empty ast', () => {
            // arrange
            const tokens = lex('hey\nhoi');

            // act
            const ast = parse(tokens);
    
            // assert
            expect(ast).not.toBeNull();
            expect(ast).not.toBeUndefined();
            expect(ast.sections.length).toBe(0);
        });
    });
});