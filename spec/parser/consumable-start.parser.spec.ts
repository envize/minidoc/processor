import { parserSuite, isolatedSuite } from '../suites';
import { createContext } from './parsing';
import ConsumableStartParser from '../../src/parser/subparsers/ConsumableStartParser';

describe(parserSuite, () => {

    describe('consumable-start', () => {

        describe(parserSuite, () => {

            test('expect linebreak token to be parsed', () => {
                // arrange
                const parser = new ConsumableStartParser(createContext(`\n`));

                // act
                const result = parser.parse(undefined);

                // assert
                expect(result.shouldBeIntegrated).toBeFalsy();
                expect(result.breakFromMainLoop).toBeTruthy();
                expect(result.node).toBeNull();
            });

            test('expect indent token to be parsed', () => {
                // arrange
                const parser = new ConsumableStartParser(createContext(`    `));

                // act
                const result = parser.parse(undefined);

                // assert
                expect(result.shouldBeIntegrated).toBeFalsy();
                expect(result.breakFromMainLoop).toBeTruthy();
                expect(result.node).toBeNull();
            });

            test('expect indent and newline tokens to be parsed', () => {
                // arrange
                const parser = new ConsumableStartParser(createContext(`\n        \n    \n`));

                // act
                const result = parser.parse(undefined);

                // assert
                expect(result.shouldBeIntegrated).toBeFalsy();
                expect(result.breakFromMainLoop).toBeTruthy();
                expect(result.node).toBeNull();
            });

            test('expect free-text token not to be parsed', () => {
                // arrange
                const parser = new ConsumableStartParser(createContext(`hey`));

                // act
                const result = parser.parse(undefined);

                // assert
                expect(result.shouldBeIntegrated).toBeFalsy();
                expect(result.breakFromMainLoop).toBeFalsy();
                expect(result.node).toBeUndefined();
            });
        });
    });
});
