import { integratorSuite } from '../suites';
import { parserSuite } from '../suites';
import EndParser from '../../src/parser/subparsers/EndParser';
import FlowSectionNode from '../../src/parser/nodes/FlowSectionNode';
import FlowType from '../../src/parser/nodes/FlowType';
import { createContext } from './parsing';

const end = 'end';
const id = '@id';

function createParentNode() {
    return new FlowSectionNode(FlowType.AlternateFlow, 'test flow');
}

describe(parserSuite, () => {

    describe('end', () => {

        describe(integratorSuite, () => {

            test('expect end link to be integrated when parsed successfully', () => {
                // arrange
                const parser = new EndParser(createContext(`${end} ${id}`));
                const result = parser.parse(undefined);
                const parentNode = createParentNode();

                // act
                parser.integrate(result, parentNode);

                // assert
                expect(parentNode.end).not.toBeNull();
                expect(parentNode.end).toStrictEqual(result.node);
            });

            test('expect end link not to be integrated when not parsed', () => {
                // arrange
                const parser = new EndParser(createContext('invalid'));
                const result = parser.parse(undefined);
                const parentNode = createParentNode();

                // act
                parser.integrate(result, parentNode);

                // assert
                expect(parentNode.end).toBeNull();
            });
        });
    });
});
