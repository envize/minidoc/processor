import { parserSuite, isolatedSuite } from '../suites';
import { createContext as createContextBase } from './parsing';
import StepType from '../../src/parser/nodes/StepType';
import StepParser from '../../src/parser/subparsers/StepParser';
import Token from '../../src/lexer/Token';
import TokenType from '../../src/lexer/TokenType';

function createContext(input: string) {
    const context = createContextBase(' ' + input);
    context.tokens.unshift(new Token(TokenType.NewLine, '\n', 0));
    context.position++;

    return context;
}

const stepTypes = [
    { type: StepType.Default, text: 'default' },
    { type: StepType.Action, text: 'action' }
];

describe(parserSuite, () => {

    describe('step', () => {

        describe(parserSuite, () => {

            test('expect step to be parsed correctly', () => {
                // arrange
                const parser = new StepParser(createContext(`step one`));

                // act
                const result = parser.parse(undefined);

                // assert
                expect(result.shouldBeIntegrated).toBeTruthy();
                expect(result.breakFromMainLoop).toBeTruthy();
                expect(result.node).not.toBeUndefined();
                expect(result.node).not.toBeNull();
                expect(result.node.type).toBe(StepType.Default);
                expect(result.node.identifier).toBe(null);
                expect(result.node.description).toBe('step one');
            });

            test.each(stepTypes)
                (`expect step type to be parsed correctly`, stepType => {
                    // arrange
                    const parser = new StepParser(createContext(`${stepType.text} step one`));

                    // act
                    const result = parser.parse(undefined);

                    // assert
                    expect(result.shouldBeIntegrated).toBeTruthy();
                    expect(result.breakFromMainLoop).toBeTruthy();
                    expect(result.node).not.toBeUndefined();
                    expect(result.node).not.toBeNull();
                    expect(result.node.type).toBe(stepType.type);
                    expect(result.node.identifier).toBe(null);
                    expect(result.node.description).toBe('step one');
                });

            test.each(stepTypes)
                (`expect second step type to be part of description`, stepType => {
                    // arrange
                    const parser = new StepParser(createContext(`${stepType.text} ${stepType.text} step one`));

                    // act
                    const result = parser.parse(undefined);

                    // assert
                    expect(result.shouldBeIntegrated).toBeTruthy();
                    expect(result.breakFromMainLoop).toBeTruthy();
                    expect(result.node).not.toBeUndefined();
                    expect(result.node).not.toBeNull();
                    expect(result.node.type).toBe(stepType.type);
                    expect(result.node.identifier).toBe(null);
                    expect(result.node.description).toBe(stepType.text + ' step one');
                });

            test.each(stepTypes)
                (`expect character after step type to be part of description`, stepType => {
                    // arrange
                    const parser = new StepParser(createContext(`${stepType.text}n step one`));

                    // act
                    const result = parser.parse(undefined);

                    // assert
                    expect(result.shouldBeIntegrated).toBeTruthy();
                    expect(result.breakFromMainLoop).toBeTruthy();
                    expect(result.node).not.toBeUndefined();
                    expect(result.node).not.toBeNull();
                    expect(result.node.type).toBe(StepType.Default);
                    expect(result.node.identifier).toBe(null);
                    expect(result.node.description).toBe(stepType.text + 'n step one');
                });

            test('expect step identifier to be parsed correctly', () => {
                // arrange
                const parser = new StepParser(createContext(`@id step one`));

                // act
                const result = parser.parse(undefined);

                // assert
                expect(result.shouldBeIntegrated).toBeTruthy();
                expect(result.breakFromMainLoop).toBeTruthy();
                expect(result.node).not.toBeUndefined();
                expect(result.node).not.toBeNull();
                expect(result.node.type).toBe(StepType.Default);
                expect(result.node.identifier).toBe('@id');
                expect(result.node.description).toBe('step one');
            });

            test('expect second step identifier to be part of description', () => {
                // arrange
                const parser = new StepParser(createContext(`@id @identifier step one`));

                // act
                const result = parser.parse(undefined);

                // assert
                expect(result.shouldBeIntegrated).toBeTruthy();
                expect(result.breakFromMainLoop).toBeTruthy();
                expect(result.node).not.toBeUndefined();
                expect(result.node).not.toBeNull();
                expect(result.node.type).toBe(StepType.Default);
                expect(result.node.identifier).toBe('@id');
                expect(result.node.description).toBe('@identifier step one');
            });

            test.each(stepTypes)
                (`expect step identifier and type to be parsed correctly`, stepType => {
                    // arrange
                    const parser = new StepParser(createContext(`${stepType.text} @id step one`));

                    // act
                    const result = parser.parse(undefined);

                    // assert
                    expect(result.shouldBeIntegrated).toBeTruthy();
                    expect(result.breakFromMainLoop).toBeTruthy();
                    expect(result.node).not.toBeUndefined();
                    expect(result.node).not.toBeNull();
                    expect(result.node.type).toBe(stepType.type);
                    expect(result.node.identifier).toBe('@id');
                    expect(result.node.description).toBe('step one');
                });

            test.each(stepTypes)
                (`expect step with only step type to be consumed and skipped`, stepType => {
                    // arrange
                    const parser = new StepParser(createContext(stepType.text));

                    // act
                    const result = parser.parse(undefined);

                    // assert
                    expect(result.shouldBeIntegrated).toBeFalsy();
                    expect(result.breakFromMainLoop).toBeTruthy();
                    expect(result.node).toBeNull();
                });

            test('expect empty lines to be consumed and skipped', () => {
                // arrange
                const parser = new StepParser(createContext(`\n\n\n`));

                // act
                const result = parser.parse(undefined);

                // assert
                expect(result.shouldBeIntegrated).toBeFalsy();
                expect(result.breakFromMainLoop).toBeTruthy();
                expect(result.node).toBeNull();
            });

            test('expect step without natural start to be skipped', () => {
                // arrange
                const context = createContextBase('           step one');
                context.tokens.unshift(new Token(TokenType.Section, '#happy-flow', 0));
                context.position++;

                const parser = new StepParser(context);

                // act
                const result = parser.parse(undefined);

                // assert
                expect(result.shouldBeIntegrated).toBeFalsy();
                expect(result.breakFromMainLoop).toBeFalsy();
                expect(result.node).toBeUndefined();
            });

            test.each(['start', 'end'])
                (`expect link to be part of the description when no identifier specified '%s'`, linkText => {
                    // arrange
                    const parser = new StepParser(createContext(`${linkText} step one`));

                    // act
                    const result = parser.parse(undefined);

                    // assert
                    expect(result.shouldBeIntegrated).toBeTruthy();
                    expect(result.breakFromMainLoop).toBeTruthy();
                    expect(result.node).not.toBeUndefined();
                    expect(result.node).not.toBeNull();
                    expect(result.node.type).toBe(StepType.Default);
                    expect(result.node.identifier).toBe(null);
                    expect(result.node.description).toBe(linkText + ' step one');
                });
        });
    });
});
