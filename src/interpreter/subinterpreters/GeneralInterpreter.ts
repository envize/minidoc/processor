import SectionNode from "../../parser/nodes/SectionNode";
import SectionType from "../../parser/nodes/Section";
import EvalResult from "../EvalResult";
import ISubInterpreter from "./ISubInterpreter";
import MarkdownInterpreter from "./MarkdownInterpreter";
import MarkdownSectionNode from "../../parser/nodes/MarkdownSectionNode";
import { Document } from "@minidoc/core";

export default class GeneralInterpreter implements ISubInterpreter<SectionNode> {

    private markdownInterpreter: MarkdownInterpreter;

    constructor() {
        this.markdownInterpreter = new MarkdownInterpreter();
    }

    public eval(doc: Document, section: SectionNode): EvalResult {

        if (section.type === SectionType.General) {
            if (doc.general !== null) {
                return EvalResult.break;
            }

            const content = <MarkdownSectionNode>section.section;

            const markdown = this.markdownInterpreter.evalMarkdown(content);

            if (markdown !== null) {
                doc.general = markdown;
            }

            return EvalResult.break;
        }

        return EvalResult.continue;
    }

    public finalize(doc: Document, section: SectionNode): EvalResult {
        if (section.type === SectionType.General) {
            return EvalResult.break;
        }

        return EvalResult.continue;
    }
}
