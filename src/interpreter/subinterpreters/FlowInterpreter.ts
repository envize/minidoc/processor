import FlowSectionNode from '../../parser/nodes/FlowSectionNode';
import LinkNode from "../../parser/nodes/LinkNode";
import StepNode from "../../parser/nodes/StepNode";
import ParserStepType from '../../parser/nodes/StepType';
import ParserFlowType from '../../parser/nodes/FlowType';
import { Document, Flow, FlowType, Link, Step, StepType } from "@minidoc/core";

export default abstract class FlowInterpreter {
    private flowCache: Map<FlowSectionNode, Flow> = new Map();

    protected evalFlow(node: FlowSectionNode): Flow | null {
        if (node === undefined ||
            node === null ||
            (node.steps && node.steps.length == 0)) {
            return null;
        }

        const type = this.getFlowType(node.type);
        const flow = new Flow(type);

        flow.description = node.description;

        for (let i = 0; i < node.steps.length; i++) {
            const stepNode = node.steps[i];
            const step = this.convertStep(stepNode, i);
            flow.steps.push(step);
        }

        this.flowCache.set(node, flow);

        return flow;
    }

    protected setLinks(document: Document, node: FlowSectionNode): void {
        const flow = this.flowCache.get(node);

        if (flow !== undefined &&
            flow !== null &&
            document.happyFlow !== null) {

            if (node.start !== null) {
                flow.start = this.convertLink(document.happyFlow, node.start);
            }

            if (node.end !== null) {
                flow.end = this.convertLink(document.happyFlow, node.end);
            }
        }
    }

    private convertStep(node: StepNode, index: number): Step {
        const type = this.getStepType(node.type);
        const description = node.description;

        const step = new Step(type, description);
        step.position = index;
        step.id = (index + 1).toString();

        if (node.identifier !== null) {
            step.id = node.identifier.substring(1);
        }


        return step;
    }

    private convertLink(happyFlow: Flow, node: LinkNode): Link | null {
        const id = node.identifier.substring(1);
        const target = happyFlow.steps.find(s => s.id === id);

        if (target) {
            const link = new Link(target);
            link.description = node.description;

            return link;
        }

        return null;
    }

    private getFlowType(parserFlowType: ParserFlowType): FlowType {
        switch (parserFlowType) {
        case ParserFlowType.HappyFlow:
            return FlowType.HappyFlow;
        case ParserFlowType.AlternateFlow:
            return FlowType.AlternateFlow;
        }
    }

    private getStepType(parserStepType: ParserStepType): StepType {
        switch (parserStepType) {
        case ParserStepType.Default:
            return StepType.Default;
        case ParserStepType.Action:
            return StepType.Action;
        }
    }
}
