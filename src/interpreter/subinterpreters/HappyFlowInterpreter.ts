import SectionNode from "../../parser/nodes/SectionNode";
import SectionType from "../../parser/nodes/Section";
import EvalResult from "../EvalResult";
import ISubInterpreter from "./ISubInterpreter";
import FlowInterpreter from "./FlowInterpreter";
import FlowSectionNode from "../../parser/nodes/FlowSectionNode";
import { Document } from "@minidoc/core";

export default class HappyFlowInterpreter
    extends FlowInterpreter
    implements ISubInterpreter<SectionNode> {

    public eval(doc: Document, section: SectionNode): EvalResult {

        if (section.type === SectionType.HappyFlow) {
            if (doc.happyFlow !== null) {
                return EvalResult.break;
            }

            const content = <FlowSectionNode> section.section;

            const flow = this.evalFlow(content);

            if (flow !== undefined && flow !== null) {
                doc.happyFlow = flow;
                return EvalResult.break;
            }
        }

        return EvalResult.continue;
    }

    public finalize(doc: Document, section: SectionNode): EvalResult {
        if (section.type === SectionType.HappyFlow) {
            return EvalResult.break;
        }

        return EvalResult.continue;
    }
}
