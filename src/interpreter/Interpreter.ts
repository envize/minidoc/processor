import AbstractSyntaxTree from "../parser/AbstractSyntaxTree";
import { Document } from "@minidoc/core";
import TitleInterpreter from './subinterpreters/TitleInterpreter';
import StoryInterpreter from './subinterpreters/StoryInterpreter';
import GeneralInterpreter from './subinterpreters/GeneralInterpreter';
import HappyFlowInterpreter from './subinterpreters/HappyFlowInterpreter';
import AlternateFlowInterpreter from './subinterpreters/AlternateFlowInterpreter';
import ISubInterpreter from "./subinterpreters/ISubInterpreter";
import SectionNode from '../parser/nodes/SectionNode';
import EvalResult from './EvalResult';

export function evaluate(ast: AbstractSyntaxTree): Document | null {
    if (ast === null || ast === undefined) {
        throw new Error('ast cannot not be null.');
    }

    return new Interpreter(ast).eval();
}

class Interpreter {

    private readonly sectionInterpreters: ISubInterpreter<SectionNode>[] = [
        new TitleInterpreter(),
        new StoryInterpreter(),
        new GeneralInterpreter(),
        new HappyFlowInterpreter(),
        new AlternateFlowInterpreter()
    ];

    constructor(private ast: AbstractSyntaxTree) { }

    public eval(): Document | null {
        // @ts-expect-error undefined is not valid for title,
        // but because we're building the document here, it's fine
        const doc = new Document(undefined);

        this.runForEverySection((section, interpreter) => interpreter.eval(doc, section));

        this.runForEverySection((section, interpreter) => interpreter.finalize(doc, section));

        if (doc.title === undefined) {
            return null;
        }

        return doc;
    }

    private runForEverySection(
        action: (
            section: SectionNode,
            interpreter: ISubInterpreter<SectionNode>
        ) => EvalResult
    ) {
        for (const section of this.ast.sections) {
            for (const interpreter of this.sectionInterpreters) {
                if (action(section, interpreter).breakFromMainLoop) {
                    break;
                }
            }
        }
    }
}
