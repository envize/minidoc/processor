enum TokenType {
    Section = "section",
    Identifier = "identifier",
    Start = "start",
    End = "end",
    StepType = "step-type",
    FreeText = "free-text",
    NewLine = "new-line",
    Indent = "indent",
    BOF = "<BOF>",
    EOF = "<EOF>"
}

export default TokenType;
