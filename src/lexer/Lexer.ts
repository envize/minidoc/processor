import Context from './Context';
import LexerBase from './LexerBase';
import Token from './Token';
import TokenType from './TokenType';

import EndLexer from './sublexers/EndLexer';
import FreeTextLexer from './sublexers/FreeTextLexer';
import NewLineLexer from './sublexers/NewLineLexer';
import IdentifierLexer from './sublexers/IdentifierLexer';
import IndentLexer from './sublexers/IndentLexer';
import SectionLexer from './sublexers/SectionLexer';
import StartLexer from './sublexers/StartLexer';
import StepTypeLexer from './sublexers/StepTypeLexer';
import WhitespaceLexer from './sublexers/WhitespaceLexer';
import ISubLexer from './sublexers/ISubLexer';

export function lex(input: string): Token[] {
    if (input === null || input === undefined) {
        throw new Error('input cannot not be null.');
    }

    return new Lexer(input).lex();
}

class Lexer extends LexerBase {
    private sublexers: ISubLexer[];

    constructor(input: string) {
        super(new Context([ ...input ]));

        this.sublexers = [
            new IndentLexer(this.context),
            new WhitespaceLexer(this.context),
            new NewLineLexer(this.context),
            new SectionLexer(this.context),
            new StartLexer(this.context),
            new EndLexer(this.context),
            new IdentifierLexer(this.context),
            new StepTypeLexer(this.context),
            new FreeTextLexer(this.context)
        ];
    }

    public lex(): Token[] {
        while (!this.isEof()) {
            for (const sublexer of this.sublexers) {
                if (sublexer.lex().breakFromMainLoop) {
                    break;
                }
            }
        }

        this.context.tokens.push(new Token(TokenType.EOF, '<EOF>', this.context.position));

        return this.context.tokens;
    }
}
