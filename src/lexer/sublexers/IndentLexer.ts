import LexerBase from "../LexerBase";
import Token from "../Token";
import TokenType from "../TokenType";
import ISubLexer from "./ISubLexer";
import LexResult from "./LexResult";

const indentSize = 4;

export default class IndentLexer extends LexerBase implements ISubLexer {

    public lex(): LexResult {
        const previousToken = this.getPreviousToken();

        if (this.isNaturalStart(previousToken) ||
            previousToken.type === TokenType.Indent) {

            let index = 0;
            while (index < indentSize &&
                this.isWhitespace(this.lookAhead(index))) {
                index++;
            }

            if (index === indentSize) {
                const position = this.context.position;
                let text = '';

                for (let i = 0; i < indentSize; i++) {
                    text += this.consume();
                }

                this.context.tokens.push(new Token(TokenType.Indent, text, position));
                return LexResult.break;
            }
        }

        return LexResult.continue;
    }
}
