import LexResult from "./LexResult";

export default interface ISubLexer {
    lex(): LexResult;
}
