import LexerBase from "../LexerBase";
import Token from "../Token";
import TokenType from "../TokenType";
import ISubLexer from "./ISubLexer";
import LexResult from "./LexResult";

export default abstract class MarkerLexer extends LexerBase implements ISubLexer {
    protected abstract readonly marker: string;
    protected abstract readonly tokenType: TokenType;

    public lex(): LexResult {
        if (this.current === this.marker) {
            let sectionIsValid = true;
            let delta = 1;
            while (!this.isEof(delta)) {
                const character = this.lookAhead(delta);
                if (!this.isValidCharacter(character)) {
                    if (!this.isNaturalEnding(character)) {
                        sectionIsValid = false;
                    }

                    break;
                }

                delta++;
            }

            // only valid characters until natural ending, and at least one character long
            if (sectionIsValid && delta > 1) {
                const position = this.context.position;

                let text = this.consume(); // consume marker
                for (let i = 0; i < delta - 1; i++) {
                    text += this.consume();
                }

                this.context.tokens.push(new Token(this.tokenType, text, position));
                return LexResult.break;
            }
        }

        return LexResult.continue;
    }


    private isValidCharacter(character: string): boolean {
        return (character >= 'a' && character <= 'z') || // is lowercase alpha
            (character >= 'A' && character <= 'Z') || // is uppercase alpha
            (character >= '0' && character <= '9') || // is digit
            character === '-' || // is dash
            character === '_'; // is underscore
    }
}
