import LexerBase from "../LexerBase";
import Token from "../Token";
import TokenType from "../TokenType";
import ISubLexer from "./ISubLexer";
import LexResult from "./LexResult";

export default abstract class KeywordLexer extends LexerBase implements ISubLexer {
    protected abstract readonly keywords: string[];
    protected abstract readonly tokenType: TokenType;

    public lex(): LexResult {
        const matchedKeyword = this.getMatchingKeyword();
        if (!matchedKeyword) {
            return LexResult.continue;
        }

        const position = this.context.position;
        let text = '';
        for (let i = 0; i < matchedKeyword.length; i++) {
            text += this.consume();
        }

        this.context.tokens.push(new Token(this.tokenType, text, position));
        return LexResult.break;
    }

    private getMatchingKeyword(): string | null {
        for (const keyword of this.keywords) {
            let isMatch = true;

            for (let i = 0; i < keyword.length; i++) {
                const character = this.lookAhead(i);

                if (character !== keyword.charAt(i)) {
                    isMatch = false;
                    break;
                }
            }

            if (isMatch && (
                this.isEof(keyword.length) ||
                this.isNaturalEnding(this.lookAhead(keyword.length)))) {
                return keyword;
            }
        }

        return null;
    }
}
