import LexerBase from "../LexerBase";
import ISubLexer from "./ISubLexer";
import LexResult from "./LexResult";

export default class WhitespaceLexer extends LexerBase implements ISubLexer {

    public lex(): LexResult {
        if (this.isWhitespace(this.current)) {
            this.consume();
            return LexResult.break;
        }

        return LexResult.continue;
    }
}
