import TokenType from "../TokenType";
import ISubLexer from "./ISubLexer";
import LexResult from "./LexResult";
import KeywordLexer from "./KeywordLexer";

export default class SectionLexer extends KeywordLexer implements ISubLexer {
    protected override readonly keywords = [
        '#title',
        '#general',
        '#story',
        '#happy-flow',
        '#alternate-flow'
    ];
    protected override readonly tokenType = TokenType.Section;

    public lex(): LexResult {
        if (this.isNaturalStart(this.getPreviousToken())) {
            return super.lex();
        }

        return LexResult.continue;
    }
}
