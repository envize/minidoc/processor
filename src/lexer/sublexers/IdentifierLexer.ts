import TokenType from "../TokenType";
import ISubLexer from "./ISubLexer";
import LexResult from "./LexResult";
import MarkerLexer from "./MarkerLexer";

export default class IdentifierLexer extends MarkerLexer implements ISubLexer {
    protected override readonly marker = '@';
    protected override readonly tokenType = TokenType.Identifier;

    public lex(): LexResult {
        return super.lex();
    }
}
