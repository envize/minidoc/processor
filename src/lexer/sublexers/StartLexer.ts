import TokenType from "../TokenType";
import ISubLexer from "./ISubLexer";
import KeywordLexer from "./KeywordLexer";
import LexResult from "./LexResult";

export default class StartLexer extends KeywordLexer implements ISubLexer {
    protected override readonly keywords = [ 'start' ];
    protected override readonly tokenType = TokenType.Start;

    public lex(): LexResult {
        if (this.isNaturalStart(this.getPreviousToken())) {
            return super.lex();
        }

        return LexResult.continue;
    }
}
