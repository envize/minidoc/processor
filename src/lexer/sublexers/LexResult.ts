export default class LexResult {

    public static get break(): LexResult {
        return new LexResult(true);
    }

    public static get continue(): LexResult {
        return new LexResult(false);
    }

    private constructor(public breakFromMainLoop: boolean) {}
}
