import SectionNode from './nodes/SectionNode';

export default class AbstractSyntaxTree {
    public sections: SectionNode[] = [];
}
