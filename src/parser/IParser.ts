import ParseResult from "./ParseResult";

export default interface IParser<TData, TResult> {
    parse(data: TData): ParseResult<TResult>;
}
