import Token from '../lexer/Token';

export default class Context {
    public tokens: Token[];
    public position: number;

    constructor(tokens: Token[]) {
        this.tokens = tokens;
        this.position = 0;
    }
}
