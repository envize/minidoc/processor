export default class ParseResult<T> {

    public static break<T>(node: T): ParseResult<T> {
        return new ParseResult<T>(node, true);
    }

    public static continue<T>(node: T | undefined = undefined): ParseResult<T | undefined> {
        return new ParseResult<T | undefined>(node, false);
    }

    public get shouldBeIntegrated(): boolean {
        return this.node !== undefined && this.node !== null;
    }

    constructor(
        public node: T,
        public breakFromMainLoop: boolean
    ) { }
}
