import ParserBase from '../ParserBase';
import SectionNode from '../nodes/SectionNode';
import TokenType from '../../lexer/TokenType';
import ParseResult from '../ParseResult';
import MarkdownSectionNode from '../nodes/MarkdownSectionNode';
import Token from '../../lexer/Token';
import IParser from '../IParser';
import IIntegrator from '../IIntegrator';
import SectionType from '../nodes/Section';

export default class MarkdownSectionParser
    extends ParserBase
    implements IParser<SectionType, MarkdownSectionNode | null | undefined>,
    IIntegrator<MarkdownSectionNode, SectionNode> {

    public parse(sectionType: SectionType): ParseResult<MarkdownSectionNode | null | undefined> {
        if (this.isSectionToParse(sectionType)) {

            while (!this.isNaturalEnd() &&
                this.isConsumable()) { // trim start
                this.consume();
            }

            while (this.isNaturalEnd() &&
                this.isConsumable()) { // trim newlines
                this.consume();
            }

            const markdownTokens: Token[] = [];
            while (!this.isEof() &&
                this.current.type !== TokenType.Section) {
                markdownTokens.push(this.consume());
            }

            while (markdownTokens.length > 0 &&
                this.isNewLine(markdownTokens[markdownTokens.length - 1])) { // trim end
                markdownTokens.pop();
            }

            if (markdownTokens.length > 0) {
                let content = '';
                for (let i = 0; i < markdownTokens.length; i++) {
                    const token = markdownTokens[i];
                    const nextTokenType = i < markdownTokens.length - 1 ?
                        markdownTokens[i + 1].type :
                        TokenType.NewLine;

                    content += token.text;

                    if (!this.isNewLine(token) &&
                        token.type !== TokenType.Indent &&
                        nextTokenType !== TokenType.NewLine &&
                        nextTokenType !== TokenType.Indent) {
                        content += ' ';
                    }
                }

                const node = new MarkdownSectionNode(content);

                return ParseResult.break(node);
            }

            return ParseResult.break(null);
        }

        return ParseResult.continue();
    }

    public integrate(parseResult: ParseResult<MarkdownSectionNode>, context: SectionNode): void {
        if (parseResult.shouldBeIntegrated) {
            context.section = parseResult.node;
        }
    }

    private isSectionToParse(sectionType: SectionType): boolean {
        return sectionType === SectionType.Story ||
            sectionType === SectionType.General;
    }
}
