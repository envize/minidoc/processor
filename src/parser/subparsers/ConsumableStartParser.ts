
import TokenType from '../../lexer/TokenType';
import IParser from '../IParser';
import ParserBase from '../ParserBase';
import ParseResult from '../ParseResult';

export default class ConsumableStartParser extends ParserBase implements IParser<undefined, null | undefined> {

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    parse(data: undefined): ParseResult<null | undefined> {
        let success = false;

        while (this.current.type === TokenType.NewLine ||
            this.current.type === TokenType.Indent) {
            success = true;
            this.consume();
        }

        if (success) {
            return ParseResult.break(null);
        }

        return ParseResult.continue();
    }
}
