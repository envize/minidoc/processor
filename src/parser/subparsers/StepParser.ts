import Token from "../../lexer/Token";
import TokenType from "../../lexer/TokenType";
import IIntegrator from "../IIntegrator";
import IParser from "../IParser";
import FlowSectionNode from '../nodes/FlowSectionNode';
import StepNode from "../nodes/StepNode";
import StepType from "../nodes/StepType";
import ParserBase from "../ParserBase";
import ParseResult from "../ParseResult";

export default class StepParser
    extends ParserBase
    implements IParser<undefined, StepNode | null | undefined>,
    IIntegrator<StepNode, FlowSectionNode> {

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    public parse(data: undefined): ParseResult<StepNode | null | undefined> {

        if (this.isNaturalBegin(this.getPreviousToken())) {
            let identifier: string | null = null;
            let type = StepType.Default;

            if (this.current.type === TokenType.StepType) {
                const stepTypeToken = this.consume();
                const stepType = this.getStepType(stepTypeToken.text);
                type = stepType;
            }

            if (this.current.type === TokenType.Identifier) {
                const identifierToken = this.consume();
                const id = identifierToken.text;
                identifier = id;
            }

            const descriptionTokens: Token[] = [];
            while (!this.isNaturalEnd()) {
                descriptionTokens.push(this.consume());
            }

            if (descriptionTokens.length > 0) {
                const description = descriptionTokens.map(t => t.text).join(' ');
                const step = new StepNode(description);
                step.identifier = identifier;
                step.type = type;

                return ParseResult.break(step);
            }

            return ParseResult.break(null);
        }

        return ParseResult.continue();
    }

    public integrate(parseResult: ParseResult<StepNode>, context: FlowSectionNode): void {
        if (parseResult.shouldBeIntegrated) {
            context.steps.push(parseResult.node);
        }
    }

    // @ts-expect-error no default case
    private getStepType(stepType: string): StepType {
        switch (stepType) {
        case 'default':
            return StepType.Default;
        case 'action':
            return StepType.Action;
        }
    }
}
