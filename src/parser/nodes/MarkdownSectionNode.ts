import ISectionNode from "./ISectionNode";
import SectionType from "./SectionType";

export default class MarkdownSectionNode implements ISectionNode {
    public readonly sectionType = SectionType.Markdown;

    constructor(public content: string) {}
}
