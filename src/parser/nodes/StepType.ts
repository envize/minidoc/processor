enum StepType {
    Default = "default",
    Action = "action"
}

export default StepType;
