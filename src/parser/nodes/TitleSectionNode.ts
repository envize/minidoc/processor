import ISectionNode from "./ISectionNode";
import SectionType from "./SectionType";

export default class TitleSectionNode implements ISectionNode {
    public readonly sectionType = SectionType.Title;

    constructor(public title: string) {}
}
