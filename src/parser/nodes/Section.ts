enum Section {
    Title = "title",
    General = "general",
    Story = "story",
    HappyFlow = "happy-flow",
    AlternateFlow = "alternate-flow"
}

export default Section;
