enum LinkType {
    Start = "start",
    End = "end"
}

export default LinkType;
