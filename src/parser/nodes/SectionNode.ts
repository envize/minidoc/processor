import ISectionNode from './ISectionNode';
import SectionType from './Section';

export default class SectionNode {
    constructor(
        public type: SectionType,
        public keyword: string,
        public section: ISectionNode | null = null
    ) {}
}
