import StepType from './StepType';

export default class StepNode {
    public identifier: string | null = null;
    public type: StepType = StepType.Default;

    constructor(public description: string) { }
}
