enum FlowType {
    HappyFlow = "happy-flow",
    AlternateFlow = "alternate-flow"
}

export default FlowType;
